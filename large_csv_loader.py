import csv
from pymongo import MongoClient
import urllib.parse
import os
import json

payload = []

VarUsername = urllib.parse.quote_plus('admin')
VarPassword = urllib.parse.quote_plus('pass')
client = MongoClient('mongodb://192.168.1.2:60017/', username=VarUsername,
                     password=VarPassword)
db = client['RNA']
collection = db['association']
posts = db.association

CsvFileDir = 'csv/in/'
CsvFileList = os.listdir(CsvFileDir)
print(CsvFileList)

NbLineToLoad = 10
NbLineAlreadyLoaded = 1

for csvfilename in CsvFileList:
    print(csvfilename)
    NbLineTotal = 0
    NbLineLoaded = 0
    with open(CsvFileDir + csvfilename, encoding="ANSI") as csvfile2:
        dictjcl2 = csv.DictReader(csvfile2, delimiter=';')
        NbLineTotal = len(list(dictjcl2))
    csvfile2.close()

    with open(CsvFileDir + csvfilename, encoding="ANSI") as csvfile:
        dictjcl = csv.DictReader(csvfile, delimiter=';')
        for row in dictjcl:
            if NbLineTotal >= NbLineToLoad + NbLineLoaded:
                payload.append(row)
                NbLineAlreadyLoaded = NbLineAlreadyLoaded + 1
                if NbLineAlreadyLoaded == NbLineToLoad:
                    posts.insert_many(payload)
                    NbLineAlreadyLoaded = 1
                    payload.clear()
            else:
                posts.insert_one(row).inserted_id
            NbLineLoaded = NbLineLoaded + 1
    csvfile.close()
