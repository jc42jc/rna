import csv
from pymongo import MongoClient
import urllib.parse
import os


VarUsername = urllib.parse.quote_plus('admin')
VarPassword = urllib.parse.quote_plus('pass')
CsvFileDir = 'csv/in/'
CsvFileList = os.listdir(CsvFileDir)
print(CsvFileList)

for csvfilename in CsvFileList:
    print(csvfilename)
    with open(CsvFileDir + csvfilename, encoding="ANSI") as csvfile:
        dictjcl = csv.DictReader(csvfile, delimiter=';')

        for row in dictjcl:
            client = MongoClient('mongodb://192.168.1.2:60017/', username=VarUsername,
                                 password=VarPassword)
            db = client['RNA']
            collection = db['association']
            posts = db.association
            posts.insert_one(row).inserted_id